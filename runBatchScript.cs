using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace uControl
{
    class Program
    {
        static void Main(string[] args)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + "C:\\Windows\\System32\\cmd.exe");
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;

            process = Process.Start(processInfo);
            process.WaitForExit();

            exitCode = process.ExitCode;
            process.Close();

            Console.WriteLine("ExitCode: " + exitCode.ToString());
            Console.ReadLine();
        }
    }
}
